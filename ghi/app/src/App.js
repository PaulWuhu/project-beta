import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React, { useState, useEffect} from 'react';
import TechnicianList from './TechnicianList';
import CreateTechnician from './CreateTechnician';
import CreateAppointment from './CreateAppointment';
import ServiceAppointments from './ServiceAppointments';
import ServiceHistory from './ServiceHistory';
import ListManufacturers from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import ListVehicle from './ListVehicle';
import CreateVehicle from './CreateVehicle';
import ListAuto from './ListAuto';
import CreateAuto from './CreateAuto';

function App() {
  const [technician,setTechnician] = useState([])
  const [appointment,setAppointment] = useState([])
  const [manufacturers,setManufacturers] = useState([])
  const [vehicles,setVehicles] = useState([])
  const [auto,setAuto] = useState([]) 

  const fetchTechnicianData= async () =>{ 
    try{
        const response = await fetch('http://localhost:8080/api/technicians/')
        if(response.ok){
            const technicianData=await response.json()
            setTechnician(technicianData.technicians)    
        }
        else{
            console.error(response)
        }
    }
    catch(e){
        console.log(e)
    }
}

  const fetchAppointmentData= async () =>{ 
    try{
        const response = await fetch('http://localhost:8080/api/appointments/')
        if(response.ok){
            const appointmentData=await response.json()
            setAppointment(appointmentData.appointments)    
        }
        else{
            console.error(response)
        }
    }
    catch(e){
        console.log(e)
    }
  }

  const fetchManufacturersData= async () =>{ 
    try{
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if(response.ok){
            const manufacturersData= await response.json()
            setManufacturers(manufacturersData.manufacturers)    
        }
        else{
            console.error(response)
        }
    }
    catch(e){
        console.log(e)
    }
  }

  const fetchVehiclesData= async () =>{ 
    try{
        const response = await fetch('http://localhost:8100/api/models/')
        if(response.ok){
            const vehiclesData= await response.json()
            setVehicles(vehiclesData.models)    
        }
        else{
            console.error(response)
        }
    }
    catch(e){
        console.log(e)
    }
  }

  const fetchAutoData= async () =>{ 
    try{
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if(response.ok){
            const autoData=await response.json()
            setAuto(autoData.autos)    
        }
        else{
            console.error(response)
        }
    }
    catch(e){
        console.log(e)
    }
  }

  useEffect(() => {
    fetchTechnicianData()},[]);
  useEffect(() => {
      fetchAppointmentData()},[]);
  useEffect(() => {
    fetchManufacturersData()},[]);
  useEffect(() => {
    fetchVehiclesData()},[]);
  useEffect(() => {
    console.log(typeof fetchAutoData);
    fetchAutoData()},[]);
    
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route exact path="/technicians" element={<TechnicianList technician={technician}/>}/>
          <Route exact path="/technician/create" element={<CreateTechnician fetchTechnicianData={fetchTechnicianData}/>}/>
          <Route exact path="/appointments" element={<ServiceAppointments appointment={appointment} fetchAppointmentData={fetchAppointmentData}/>}/>
          <Route exact path="/service/history" element={<ServiceHistory appointment={appointment} setAppointment={setAppointment} fetchAppointmentData={fetchAppointmentData}/>}/>
          <Route exact path="/appointment/create" element={<CreateAppointment technician={technician} fetchAppointmentData={fetchAppointmentData}/>}/>
          <Route exact path="/manufacturers" element={<ListManufacturers manufacturers={manufacturers}/>}/>
          <Route exact path="/manufacturer/create" element={<CreateManufacturer fetchManufacturersData={fetchManufacturersData}/>}/>
          <Route exact path="/vehicles" element={<ListVehicle vehicles={vehicles}/>}/>
          <Route exact path="/vehicle/create" element={<CreateVehicle fetchVehiclesData={fetchVehiclesData} manufacturers={manufacturers}/>}/>
          <Route exact path="/autos" element={<ListAuto auto={auto}/>}/>
          <Route exact path="/auto/create" element={<CreateAuto fetchAutoData={fetchAutoData} vehicles={vehicles}/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
