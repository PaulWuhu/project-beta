import React from 'react';

const TechnicianList = ({technician}) => {
  return (
  <>
    <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> Here are our Technicians! </div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th> First Name</th>
                <th> Last Name</th>
                <th> Employee ID</th>
            </tr>
        </thead>
        <tbody>
            {technician.map((technician) =>(
            <tr key={technician.employee_id}>
            <td>{technician.first_name}</td>
            <td>{technician.last_name}</td>
            <td>{technician.employee_id}</td>
            </tr>
            ))}
        </tbody>
    </table>
  </>
  )
}

export default TechnicianList
