import React, { useState } from 'react';

const CreateAuto = ({ vehicles, fetchAutoData }) => {

  const [newAuto,setNewAuto]=useState({
      color:"",
      year:"",
      vin:"",
  })
  const [modelId,setModelId] = useState("")
    
  const handleFormChange = (event) => {
      const { name, value } = event.target;
      setNewAuto((prevValues) => ({
        ...prevValues,
        [name]: value,
      }));
    };

  const handleModelChange = (e)=>{
      const value = e.target.value
      setModelId(value)
  }

  const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.color=newAuto.color
      data.year=newAuto.year
      data.vin=newAuto.vin
      data.model_id = modelId
      console.log(data)
      const fetchConfig = {
          method:'post',
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
              },
      }

    const url = "http://localhost:8100/api/automobiles/"
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        const newAutoResponse = await response.json();
        setNewAuto({
        color:"",
        year:"",
        vin:"",
    })
    setModelId("")
    console.log(newAutoResponse)
    fetchAutoData()
    }
    else{
        console.log(typeof fetchAutoData)
        console.error("response not ok") 
    }
    }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Auto</h1>
            <form onSubmit={handleSubmit}id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={newAuto.color} placeholder="Color" name="color" required type="text"  className="form-control"/>
                <label htmlFor="first_name">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={newAuto.year}placeholder="Year" name="year" required type="text" className="form-control"/>
              <label htmlFor="last_name">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={newAuto.vin}placeholder="vin" name="vin" required type="text" className="form-control"/>
              <label htmlFor="last_name">Vin</label>
            </div>
            <div className="mb-3">
              <select onChange={handleModelChange} value={modelId} required name="Technician" className="form-select">
                <option value="">Select a Model</option>
                {vehicles.map(vehicle => {
                      return (
                      <option key={vehicle.id} value={vehicle.id}>
                          {vehicle.name} 
                      </option>
                      );
                  })}
              </select>
            </div>
            <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateAuto
