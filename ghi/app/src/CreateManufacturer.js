import React, { useState } from 'react';


const CreateManufacturer = ( {fetchManufacturersData} ) => {
    const [newManufacturer,setNewManufacturer]=useState('')
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = newManufacturer
        const fetchConfig = {
            method:'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        }
        const technicianURL = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
          const newManufacturer = await response.json();
          setNewManufacturer('')
          console.log(newManufacturer)
          fetchManufacturersData()
        }
        else{
            console.error("response not ok")
        }
      }
      const handleChange = (e)=>{
        const value = e.target.value
        setNewManufacturer(value)
      }
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
         <div className="shadow p-4 mt-4">
          <h1>Create New Manufacturer</h1>
          <form onSubmit={handleSubmit}id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleChange} value={newManufacturer} placeholder="" name="manufacturer" required type="text"  className="form-control"/>
              <label htmlFor="first_name"> New Manufacturer</label>
            </div>
            <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateManufacturer
