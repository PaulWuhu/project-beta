import React, { useState } from 'react';

const CreateAppointment = ({ technician, fetchAppointmentData }) => {

const [appointmentForm , setAppointmentForm] = useState({
  vin:"",
  customer:"",
  date_time:"",
  reason:"",
})
const [selectTechnician,setSelectTechnician]=useState()

const handleTechnician = (e)=>{
  const value = e.target.value
  setSelectTechnician(value)
}

const handleFormChange = (event) => {
    const { name, value } = event.target;
    setAppointmentForm((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.status = "created"
    data.vin = appointmentForm.vin
    data.customer = appointmentForm.customer
    data.date_time=appointmentForm.date_time
    data.reason = appointmentForm.reason
    data.technician = selectTechnician
    const fetchConfig = {
        method:'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
          },
    }

    const appointmentURL = "http://localhost:8080/api/appointments/"
    const response = await fetch(appointmentURL, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setAppointmentForm({     
        vin:"",
        customer:"",
        date_time:"",
        reason:"",
    })
    setSelectTechnician("")
    fetchAppointmentData()
    console.log(newAppointment)
    }
    else{
        console.error("response not ok")
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Appointment</h1>
            <form onSubmit={handleSubmit}id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={appointmentForm.vin} placeholder="Vin Number" name="vin" required type="text"  className="form-control"/>
                <label htmlFor="fabric">Vin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={appointmentForm.customer}placeholder="Customer Name" name="customer" required type="text" className="form-control"/>
                <label htmlFor="picture_url">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="mm/dd/yyyy" value={appointmentForm.date_time} name="date_time" type="datetime-local" required  className="form-control"/>
                <label htmlFor="color">Appointment Date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="Reason">Reason</label>
                <input onChange={handleFormChange} placeholder="Reason For Service" name="reason" value={appointmentForm.reason} required type="textarea"  className="form-control"/>
              </div>
              <div className="mb-3">
                <select onChange={handleTechnician} value={selectTechnician} required name="Technician" className="form-select">
                  <option value="">Choose a Technician to Help You</option>
                  {technician.map(technician => {
                        return (
                        <option key={technician.employee_id} value={technician.employee_id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateAppointment
