import React, { useState } from 'react';


const ServiceHistory = ({ appointment, setAppointment,fetchAppointmentData }) => {
    const [search,setSearch]=useState('')

    const handleSearch = (e)=>{
        const value=e.target.value
        setSearch(value)

    }

    const handleSubmit=(e)=>{
        e.preventDefault()
        const searchResult = appointment.filter(appointment => {
           return appointment.vin.toLowerCase() === search.toLowerCase()
        })
        setAppointment(searchResult)
    }

    const handleReset=(e)=>{
        e.preventDefault()
        setSearch('')
        fetchAppointmentData()
    }

  return (
    <>
    <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-4"> All Service </div>
    <form onSubmit={handleSubmit}>
        <div className="form-floating mb-3">
            <input onChange={handleSearch} value={search}placeholder="Search by Vin...." name="search" required type="text" className="form-control"/>
            <label htmlFor="search">Search by Vin....</label>
            <button type="submit">Search</button>
            <button type="reset" onClick={handleReset}>Click to Reset</button>
        </div>
    </form>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Vin</th>
                <th>VIP</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            {appointment.map((appointment) =>(
            <tr key={appointment.id}>
            <td>{appointment.vin}</td>
            <td>{appointment.is_vip.toString().toUpperCase()}</td>
            <td>{appointment.customer}</td>
            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
            <td>{appointment.reason}</td>
            <td>{appointment.status}</td>
            </tr>
            ))}
        </tbody>
    </table>
    </>
  )
}

export default ServiceHistory
