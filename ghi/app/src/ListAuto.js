import React from 'react';

const ListAuto = ({auto}) => {
  return (
    <>    
    <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> Here are our Vehicle Models! </div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturers</th>
                <th>Sold</th>
            </tr>
        </thead>
        <tbody>
            {auto.map((auto) =>(
            <tr key={auto.id}>
            <td>{auto.vin}</td>
            <td>{auto.color}</td>
            <td>{auto.year}</td>
            <td>{auto.model.name}</td>
            <td>{auto.model.manufacturer.name}</td>
            <td>{auto.sold.toString()}</td>
            </tr>
            ))}
        </tbody>
    </table>
</>
  )
}

export default ListAuto
