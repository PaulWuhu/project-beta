import React, { useState } from 'react';

const CreateVehicle = ( { fetchVehiclesData, manufacturers } ) => {

  const [vehiclesForm , setVehiclesForm] = useState({
    name:"",
    picture_url:"",
  })

  const [selectManufacturer,setSelectManufacturer] = useState("")

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setVehiclesForm((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };

  const handleSelectManufacturer = (e)=>{
    const value = e.target.value
    setSelectManufacturer(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.picture_url=vehiclesForm.picture_url
    data.manufacturer_id=selectManufacturer
    data.name=vehiclesForm.name
    const fetchConfig = {
        method:'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
          },
    }
    const modelsURL = "http://localhost:8100/api/models/"
    const response = await fetch(modelsURL, fetchConfig);
    if (response.ok) {
      const newTeach = await response.json();
      setVehiclesForm({        
        name:"",
        picture_url:"",
    })
      setSelectManufacturer("")
      fetchVehiclesData()
    console.log(newTeach)
    }
    else{
        console.error("response not ok")
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Vehicle Models</h1>
            <form onSubmit={handleSubmit}id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={vehiclesForm.name} placeholder="Name" name="name" required type="text"  className="form-control"/>
                <label htmlFor="first_name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={vehiclesForm.picture_url}placeholder="picture_url" name="picture_url" required type="text" className="form-control"/>
                <label htmlFor="last_name">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleSelectManufacturer} value={selectManufacturer} required name="Technician" className="form-select">
                  <option value="">Select a Manufacturer</option>
                  {manufacturers.map(manufacturer => {
                        return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name} 
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
            </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateVehicle
