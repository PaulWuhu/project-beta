import React from 'react'

const ListManufacturers = ( {manufacturers} ) => {
  return (
    <>    
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> Here are our Manufacturers! </div>
        <table className="table table-striped">
          <thead>
              <tr>
                  <th>manufacturers</th>
              </tr>
          </thead>
          <tbody>
              {manufacturers.map((manufacturer) =>(
              <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
              </tr>
              ))}
          </tbody>
      </table>
    </>
  )
}

export default ListManufacturers
