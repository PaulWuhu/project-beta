import React from 'react'

const ListVehicle = ({vehicles} ) => {
  return (
    <>    
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> Here are our Vehicle Models! </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturers</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {vehicles.map((vehicle) =>(
                <tr key={vehicle.id}>
                <td>{vehicle.name}</td>
                <td>{vehicle.manufacturer.name}</td>
                <td><img alt={vehicle.id} style={{"maxWidth":"300px","maxHeight":"300px"}} src={vehicle.picture_url}/></td>
                </tr>
                ))}
            </tbody>
        </table>
    </>
  )
}

export default ListVehicle
