import React, { useState } from 'react';


const CreateTechnician = ({fetchTechnicianData}) => {
  const [technicianForm , setTechnicianForm] = useState({
      first_name:"",
      last_name:"",
      employee_id:0,
  })

  const handleFormChange = (event) => {
      const { name, value } = event.target;
      setTechnicianForm((prevValues) => ({
        ...prevValues,
        [name]: value,
      }));
    };

  const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.first_name=technicianForm.first_name
      data.last_name=technicianForm.last_name
      data.employee_id=technicianForm.employee_id
      const fetchConfig = {
          method:'post',
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
            },
      }
      const technicianURL = "http://localhost:8080/api/technicians/"
      const response = await fetch(technicianURL, fetchConfig);
      if (response.ok) {
        const newTeach = await response.json();
        setTechnicianForm({        
          first_name:"",
          last_name:"",
          employee_id:"",
      })
      console.log(newTeach)
      fetchTechnicianData()
      }
      else{
          console.error("response not ok")
      }
    }

  return (
<div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Technician</h1>
          <form onSubmit={handleSubmit}id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={technicianForm.first_name} placeholder="First Name" name="first_name" required type="text"  className="form-control"/>
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={technicianForm.last_name}placeholder="Last Name" name="last_name" required type="text" className="form-control"/>
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder=">Employee ID" value={technicianForm.employee_id} type="number" name="employee_id" required className="form-control"/>
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateTechnician
