import React, { useState, useEffect } from 'react';


const ServiceAppointments = ({ appointment ,fetchAppointmentData }) => {

    const [activeAppointment, setActiveAppointment] = useState([])

    useEffect(() => {
    setActiveAppointment(appointment.filter(appointment =>{
        return appointment.status=== "created"
    }))},[appointment]);

    const fetchConfig = {
        method:'PUT',
        headers: {
            'Content-Type': 'application/json',
          },
    }

    const handleCancel=(e)=>{
        const id = e.target.value
        const fetchCancelData= async () =>{ 
            try{
                const appointmentChangeUrl = `http://localhost:8080/api/appointments/${id}/cancel`
                const response = await fetch(appointmentChangeUrl, fetchConfig);
                if(response.ok){
                    fetchAppointmentData()
                    setActiveAppointment(appointment.filter(appointment =>{
                        return appointment.status=== "created"
                    }))
                }
                else{
                    console.error(response)
                }
            }
            catch(e){
                console.log(e)
            }
        }
        fetchCancelData()
    }

    const handleFinish=(e)=>{
        const id = e.target.value
        const fetchFinishData= async () =>{ 
            try{
                const appointmentChangeUrl = `http://localhost:8080/api/appointments/${id}/finish`
                const response = await fetch(appointmentChangeUrl, fetchConfig);
                if(response.ok){
                    fetchAppointmentData()
                    setActiveAppointment(appointment.filter(appointment =>{
                        return appointment.status=== "created"
                    }))
                }
                else{
                    console.error(response)
                }
            }
            catch(e){
                console.log(e)
            }
        }
        fetchFinishData()
    }

  return (
    <>
    <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-4"> All Active Service </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>VIP</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {activeAppointment.map((appointment) =>( 
                <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.is_vip.toString().toUpperCase()}</td>
                    <td>{appointment.customer}</td>
                    <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                    <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td><button value={appointment.id} onClick={handleCancel}className="btn btn-danger" type="button">Cancel</button><button value={appointment.id} onClick={handleFinish} className="btn btn-success"type="button">Finish</button></td>
                </tr>
                ))}
            </tbody>
        </table>
    </>
  )
}

export default ServiceAppointments
