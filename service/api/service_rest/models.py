from django.db import models
from django.urls import reverse

class AutomobileVO (models.Model):
    vin = models.CharField(max_length=200, unique=True)
    import_href = models.CharField(max_length=200, null=True)

class Technician (models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.IntegerField(unique=True)
    
class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    reason = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT,
        )
    is_vip = models.BooleanField(default=False)
    status = models.CharField(max_length=10)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})
           
           