from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Technician,Appointment
from .encoder import AutomobileVOEncoder, TechnicianEncoder, AppointmentEncoder

@require_http_methods(['GET', "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians":technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid technician creation, try again"},
                status = 400
            )

@require_http_methods(["GET","DELETE"])
def api_delete_technicians(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid technician entered, try again"},
                status=404,
            )
    else:
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(['GET', "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "This employee does not exist, try again"},
                status=400,
            )
        try:
            vin = content["vin"]
            vin = AutomobileVO.objects.get(vin=vin)
            content["is_vip"] = True
        except AutomobileVO.DoesNotExist:
            content["is_vip"] = False
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid appointment input, try again"},
                status=400,
            )
        
@require_http_methods(['DELETE', "PUT"])
def api_show_appointment(request,id):
    if request.method == "DELETE": 
        count, _ = Appointment.objects.filter(id=id).delete()
        if count == 0: 
            return JsonResponse(
            {"error": "Invalid appointment input, try again"},
            status=404,
            )
        return JsonResponse({"deleted": count > 0})

    else: 
        try:
            current_url=request.build_absolute_uri()
            if current_url[-6:] == 'cancel':
                content= {
                "status": "canceled"
                }
                Appointment.objects.filter(id=id).update(**content)
                appointment = Appointment.objects.get(id=id)
                return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                )
            if current_url[-6:] == 'finish':
                content= {
                "status": "finished"
                }
                Appointment.objects.filter(id=id).update(**content)
                appointment = Appointment.objects.get(id=id)
                return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                )
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"error": "Invalid appointment input, try again"},
                status=400,
            )
